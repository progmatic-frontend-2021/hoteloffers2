import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-top-items',
  templateUrl: './featured-items.component.html',
  styleUrls: ['./featured-items.component.scss']
})
export class FeaturedItemsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
