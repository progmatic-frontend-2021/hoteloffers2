import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardNavComponent } from './dashboard-nav/dashboard-nav.component';
import { OffersListComponent } from './offers-list/offers-list.component';
import { NewOffersComponent } from './new-offers/new-offers.component';
import { EditOffersComponent } from './edit-offers/edit-offers.component';
import { OfferItemComponent } from './offers-list/offer-item/offer-item.component';



@NgModule({
  declarations: [
    DashboardComponent,
    DashboardNavComponent,
    OffersListComponent,
    NewOffersComponent,
    EditOffersComponent,
    OfferItemComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DashboardModule { }
